const Rating = require('../models').ratings;

module.exports.findMovieRatings = async (source, value, movieId) => {
    return await Rating.findOne({
        where: {
            source,
            value,
            movieId
        }
    });
};

module.exports.createMovieRating = async (source, value, movieId) => {
    return await Rating.create({
        source,
        value,
        movieId
    });
};
