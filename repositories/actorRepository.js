const Actor = require('../models').actors;
const MovieActors = require('../models').movieActors;

module.exports.findActor = async (firstName, secondName) => {
    return await Actor.findOne({
        where: {
            firstName,
            secondName
        }
    });
};

module.exports.createActor = async (firstName, secondName) => {
    return await Actor.create({
        firstName,
        secondName
    });
};

module.exports.checkIfMovieActor = async (actorId, movieId) => {
    const allocation = await MovieActors.findOne({
        where: {
            actorId,
            movieId
        }
    });

    return allocation;
};

module.exports.actorForMovie = async (actorId, movieId) => {
    return await MovieActors.create({
        actorId,
        movieId
    });
};
