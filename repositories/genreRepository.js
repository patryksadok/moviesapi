const Genre = require('../models').genres;
const MovieGenre = require('../models').movieGenres;

module.exports.findGenre = async (type) => {
    return await Genre.findOne({
        where: {
            type
        }
    });
};

module.exports.createGenre = async (type) => {
    return await Genre.create({
        type
    });
};

module.exports.checkIfMovieGenre = async (genreId, movieId) => {
    const allocation = await MovieGenre.findOne({
        where: {
            genreId,
            movieId
        }
    });

    return allocation;
};

module.exports.genreForMovie = async (genreId, movieId) => {
    return await MovieGenre.create({
        genreId,
        movieId
    });
};
