const Production = require('../models').productions;

module.exports.findProduction = async (name) => {
    return await Production.findOne({
        where: {
            name
        }
    });
};

module.exports.createProduction = async (name) => {
    return await Production.create({
        name
    });
};
