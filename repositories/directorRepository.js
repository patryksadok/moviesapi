const Director = require('../models').directors;

module.exports.findDirector = async (firstName, secondName) => {
    return await Director.findOne({
        where: {
            firstName,
            secondName
        }
    });
};

module.exports.createDirector = async (firstName, secondName) => {
    return await Director.create({
        firstName,
        secondName
    });
};
