const Award = require('../models').awards;

module.exports.findAwardByValue = async (type, movieId) => {
    return await Award.findOne({
        where: {
            type,
            movieId
        }
    });
};

module.exports.createAward = async (type, movieId) => {
    return await Award.create({
        type,
        movieId
    });
};
