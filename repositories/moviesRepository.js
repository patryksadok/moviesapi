const Movie = require('../models').movies;

module.exports.findMovie = async (imdbId) => {
    return await Movie.findOne({
        where: {
            imdbId
        }
    });
};

module.exports.findMovieById = async (id) => {
    return await Movie.findOne({
        where: {
            id
        }
    });
};
