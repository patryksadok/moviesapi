const HttpStatus = require('../config/httpStatus');

class MovieNotFoundError extends Error {

    constructor(message, status, type) {
        super();

        Error.captureStackTrace(this, this.constructor);

        this.name = this.constructor.name;

        this.type = type || 'Movie-not-found-error';

        this.message = message ||
            'Movie not found in db';

        this.status = status || HttpStatus.HTTP_404_NOT_FOUND;
    }
}

module.exports = MovieNotFoundError;
