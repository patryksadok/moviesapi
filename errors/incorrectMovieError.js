const HttpStatus = require('../config/httpStatus');

class IncorrectMovieError extends Error {

    constructor(message, status, type) {
        super();

        Error.captureStackTrace(this, this.constructor);

        this.name = this.constructor.name;

        this.type = type || 'incorrect-movie-id-error';

        this.message = message ||
            'There is no any movie in OMD api with that ID';

        this.status = status || HttpStatus.HTTP_404_NOT_FOUND;
    }
}

module.exports = IncorrectMovieError;
