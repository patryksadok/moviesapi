const HttpStatus = require('../config/httpStatus');

class MovieAlreadyExists extends Error {

    constructor(message, status, type) {
        super();

        Error.captureStackTrace(this, this.constructor);

        this.name = this.constructor.name;

        this.type = type || 'movie-already-exists-error';

        this.message = message ||
            'Movie already exists in database';

        this.status = status || HttpStatus.HTTP_400_BAD_REQUEST;
    }
}

module.exports = MovieAlreadyExists;
