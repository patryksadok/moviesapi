# moviesAPI
## created as recruitment task

# To run locally, you must have met requirements:
* installed Node
* installed Docker
* free ports 3000, 3313 (can be changed in .env and docker-compose-db.yml)

# Running instructions
Use terminal able to read sh files to make sure that scripts are opened correctly.

In project folder (moviesapi), type:
```npm install```

To run API, type:
```npm run start```

To create and run DB, run scripts:
```docker_build_db.sh```
```docker_up_db.sh```

To create migrations on db, type:
```npm run migrate```

To flush db from migrations, type:
```npm run unmigrate```

# Testing endpoints
There are 4 total endpoints:

### Creating new movie
# Checks whether a movie is present in local database, if not, api fetches new movie data from OMDb API and inserts it into database.
```POST /movies``` 

Headers:
```Content-Type application/json```

Body (with example id with value tt3896198):
```
{
	"imdbId": "tt3896198"
}
```

Response will return created movie object.

## Getting all movies
### Gets all movies present in local database
```GET /movies```

## Posting new comment
### Checks whether movie is present in database and creates new comment associated to that movie
```POST /comments/:id```

Headers:
```Content-Type application/json```

Body:
```
{
	"value": "great movie indeed"
}
```
Response will return created comment object.

## Getting comments for specified movie
### Gets all comments for chosen by id movie
```GET /comments/:id```

Response will return array with comments' objects.

