const Movie = require('../models').movies;
const axios = require('axios');

module.exports.getMovies = async () => {
    return await Movie.findAll();
};

module.exports.fetchMovie = async (uri) => {
    let movie;
    await axios.get(uri).then(response => {
        movie = response.data;       
    })
    .catch(error => {
        console.log(error);
    });

    return movie;
};

module.exports.createMovie = async (movie, directorId, productionId) => {
    const createdMovie = await Movie.create({
        title: movie.Title,
        year: movie.Year,
        rated: movie.Rated,
        released: movie.Released,
        runtime: movie.Runtime,
        writer: movie.Writer,
        plot: movie.Plot,
        language: movie.Language,
        country: movie.Country,
        poster: movie.Poster,
        metascore: movie.Metascore,
        imdbRating: movie.imdbRating,
        imdbVotes: movie.imdbVotes,
        imdbId: movie.imdbID,
        type: movie.Type,
        dvd: movie.DVD,
        boxOffice: movie.BoxOffice,
        website: movie.Website,
        directorId,
        productionId,
        response: movie.Response
    });

    return createdMovie.dataValues;
};
