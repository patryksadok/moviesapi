const productionRepository = require('../repositories/productionRepository');

module.exports.checkOrCreate = async (name) => {
    console.log(name);
    const production = await productionRepository.findProduction(name);
    
    if (!production) {
        const createdProduction = await productionRepository.createProduction(name);
        return createdProduction.dataValues;
    }

    return production.dataValues;
};
