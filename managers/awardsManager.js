const awardsRepository = require('../repositories/awardsRepository');

module.exports.checkOrCreate = async (value, movieId) => {
    const foundAward = await awardsRepository.findAwardByValue(value, movieId);
    
    if (!foundAward) {
        let createdAward = await awardsRepository.createAward(value, movieId);
        return createdAward.dataValues;
    }
    
    return foundAward.dataValues;
};
