const Comment = require('../models').comments;

module.exports.getComments = async (movieId) => {
    return await Comment.findAll({
        where: {
            movieId
        }
    });
};

module.exports.createComment = async (value, movieId) => {
    const createdComment = await Comment.create({
        value,
        movieId
    });
    return createdComment.dataValues;
};