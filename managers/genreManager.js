const genreRepository = require('../repositories/genreRepository');

module.exports.checkOrCreate = async (types) => {
    const genresArray = [];
    const typesArray = types.split(', ');
    for (type of typesArray) {
        const genre = await genreRepository.findGenre(type);
        if (!genre) {
            const genre = await genreRepository.createGenre(type);
            genresArray.push(genre.dataValues);
        }
        else {
            genresArray.push(genre.dataValues);
        }
    }

    return genresArray;
};

module.exports.allignGenres = async (genres, movieId) => {
    for (genre of genres) {
        const foundAllocation = await genreRepository.checkIfMovieGenre(genre.id, movieId);
        
        if (!foundAllocation) {
            await genreRepository.genreForMovie(genre.id, movieId);
        }
    }
};
