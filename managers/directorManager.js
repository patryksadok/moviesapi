const directorRepository = require('../repositories/directorRepository');

module.exports.checkOrCreate = async (director) => {
    const names = director.split(' ');
    const foundDirector = await directorRepository.findDirector(names[0], names[1]);
    if (!foundDirector) {
        let createdDirector = await directorRepository.createDirector(names[0], names[1]);
        createdDirector = createdDirector.dataValues
        return createdDirector;
    }
    
    return foundDirector.dataValues;
};
