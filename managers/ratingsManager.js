const ratingsRepository = require('../repositories/ratingsRepository');

module.exports.checkOrCreate = async (ratings, movieId) => {
    for (rating of ratings) {
        const foundRating = await ratingsRepository.findMovieRatings(rating.Source, rating.Value, movieId);
        if (!foundRating) {
            await ratingsRepository.createMovieRating(rating.Source, rating.Value, movieId);
        }
    }
};
