const actorRepository = require('../repositories/actorRepository');

module.exports.checkOrCreate = async (actors) => {
    const actorsArray = actors.split(', ');
    const actorsData = [];

    for (actor of actorsArray) {
        const actorNames = actor.split(' ');

        const foundActor = await actorRepository.findActor(actorNames[0], actorNames[1]);
        if (!foundActor) {
            const createdActor = await actorRepository.createActor(actorNames[0], actorNames[1]);
            actorsData.push(createdActor.dataValues);
        }
        else {
            actorsData.push(foundActor.dataValues);
        }
    }

    return actorsData;
};

module.exports.allignActors = async (actors, movieId) => {
    for (actor of actors) {
        const foundAllocation = await actorRepository.checkIfMovieActor(actor.id, movieId);
        
        if (!foundAllocation) {
            await actorRepository.actorForMovie(actor.id, movieId);
        }
    }
};
