'use strict';
module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('ratings', {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER
            },
            source: {
                type: Sequelize.STRING(300)
            },
            value: {
                type: Sequelize.STRING(300)
            },
            movie_id: {
                allowNull: false,
                type: Sequelize.INTEGER,
                references: {
                    model: 'movies',
                    key: 'id',
                }
            },
            created_at: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updated_at: {
                allowNull: false,
                type: Sequelize.DATE
            }
        });
    },
    down: (queryInterface, Sequelize) => {
        return queryInterface.dropTable('ratings');
    }
};
