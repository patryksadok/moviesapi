'use strict';
module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('movie_genres', {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER
            },
            genre_id: {
                allowNull: false,
                type: Sequelize.INTEGER,
                references: {
                    model: 'genres',
                    key: 'id',
                }
            },
            movie_id: {
                allowNull: false,
                type: Sequelize.INTEGER,
                references: {
                    model: 'movies',
                    key: 'id',
                }
            },
            created_at: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updated_at: {
                allowNull: false,
                type: Sequelize.DATE
            }
        });
    },
    down: (queryInterface, Sequelize) => {
        return queryInterface.dropTable('movie_genres');
    }
};
