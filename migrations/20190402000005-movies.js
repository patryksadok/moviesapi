'use strict';
module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('movies', {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER
            },
            title: {
                type: Sequelize.STRING(300)
            },
            year: {
                type: Sequelize.INTEGER
            },
            rated: {
                type: Sequelize.STRING(300)
            },
            released: {
                type: Sequelize.DATE
            },
            runtime: {
                type: Sequelize.STRING(300)
            },
            writer: {
                type: Sequelize.STRING(900)
            },
            plot: {
                type: Sequelize.STRING(300)
            },
            language: {
                type: Sequelize.STRING(300)
            },
            country: {
                type: Sequelize.STRING(300)
            },
            poster: {
                type: Sequelize.STRING(300)
            },
            metascore: {
                type: Sequelize.INTEGER
            },
            imdb_rating: {
                type: Sequelize.INTEGER
            },
            imdb_votes: {
                type: Sequelize.STRING(300)
            },
            imdb_id: {
                type: Sequelize.STRING(300)
            },
            type: {
                type: Sequelize.STRING(300)
            },
            dvd: {
                type: Sequelize.DATE
            },
            box_office: {
                type: Sequelize.STRING(300)
            },
            website: {
                type: Sequelize.STRING(300)
            },
            director_id: {
                allowNull: false,
                type: Sequelize.INTEGER,
                references: {
                    model: 'directors',
                    key: 'id',
                }
            },
            production_id: {
                allowNull: false,
                type: Sequelize.INTEGER,
                references: {
                    model: 'productions',
                    key: 'id',
                }
            },
            created_at: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updated_at: {
                allowNull: false,
                type: Sequelize.DATE
            }
        });
    },
    down: (queryInterface, Sequelize) => {
        return queryInterface.dropTable('movies');
    }
};
