const express = require('express');
const router = express.Router();

router.use(require('./movies'));
router.use(require('./comments'));

module.exports = router;
