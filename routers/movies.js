const express = require('express');
const router = express.Router();
const moviesController = require('../controllers/moviesController');
const moviesMiddleware = require('../middleware/checkIfMovieExist');

router.get('/movies', moviesController.getMovies);
router.post('/movies', moviesMiddleware.checkIfMovieExist, moviesController.createMovie);

module.exports = router;
