const express = require('express');
const router = express.Router();
const commentsController = require('../controllers/commentsController');
const movieMiddleware = require('../middleware/checkIfMovieNotExist');
router.get('/comments/:movieId', commentsController.getComments);
router.post('/comments/:movieId', movieMiddleware.checkIfMovieExist,  commentsController.createComment);

module.exports = router;
