const moviesManager = require('../managers/moviesManager');
const MoviesNotFoundError = require('../errors/moviesNotFoundError');
const IncorrectMovieError = require('../errors/incorrectMovieError');
const genreManager = require('../managers/genreManager');
const directorManager = require('../managers/directorManager');
const HttpStatus = require('../config/httpStatus');
const productionManager = require('../managers/productionManager');
const actorManager = require('../managers/actorManager');
const awardsManager = require('../managers/awardsManager');
const ratingsManager = require('../managers/ratingsManager');
const omdbToken = process.env.OMDB_API_KEY;

module.exports.getMovies = async (req, res) => {
    try {
        const movies = await moviesManager.getMovies();

        if (!movies || !movies.length) {
            throw new MoviesNotFoundError();
        }

        return res.status(HttpStatus.HTTP_200_OK).json({
            movies
        });
    } catch (error) {
        console.log(error);
         return res.status(error.status).json({
            message: error.message,
            type: error.type,
        });
    }
};

module.exports.createMovie = async (req, res) => {
    try {
        const { imdbId } = req.body;
        const omdbURI = `http://www.omdbapi.com/?i=${imdbId}&apikey=${omdbToken}`;
        
        const movie = await moviesManager.fetchMovie(omdbURI);

        if (movie.Error) {
            throw new IncorrectMovieError();
        }

        const {
            Genre,
            Director,
            Actors,
            Awards,
            Ratings,
            Production,
        } = movie;

        const movieDirector = await directorManager.checkOrCreate(Director);
        const productionStudio = await productionManager.checkOrCreate(Production);
        const createdMovie = await moviesManager.createMovie(movie, movieDirector.id, productionStudio.id);
        const genres = await genreManager.checkOrCreate(Genre);
        await genreManager.allignGenres(genres, createdMovie.id);
        const actors = await actorManager.checkOrCreate(Actors);
        await actorManager.allignActors(actors, createdMovie.id);
        await awardsManager.checkOrCreate(Awards, createdMovie.id);
        await ratingsManager.checkOrCreate(Ratings, createdMovie.id);

        return res.status(HttpStatus.HTTP_200_OK).json({
            createdMovie
        });
    }
    catch (error) {
        console.log(error);
         return res.status(error.status).json({
            message: error.message,
            type: error.type,
        });
    }
}
