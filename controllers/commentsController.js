const commentsManager = require('../managers/commentsManager');
const CommentsNotFoundError = require('../errors/commentsNotFoundError');
const MovieNotFoundError = require('../errors/movieNotFoundError');
const HttpStatus = require('../config/httpStatus');

module.exports.getComments = async (req, res) => {
    try {
        const { movieId } = req.params;
        const comments = await commentsManager.getComments(movieId);

        if (!comments || !comments.length) {
            throw new CommentsNotFoundError();
        }

        return res.status(HttpStatus.HTTP_200_OK).json({
            comments
        });
    } catch (error) {
        console.log(error);
         return res.status(error.status).json({
            message: error.message,
            type: error.type,
        });
    }
};

module.exports.createComment = async (req, res) => {
    try {
        const { movieId } = req.params;
        const { value } = req.body;

        const createdComment = await commentsManager.createComment(value, movieId);
        
        return res.status(HttpStatus.HTTP_200_OK).json({
            createdComment
        });
    } catch (error) {
        console.log(error);
         return res.status(error.status).json({
            message: error.message,
            type: error.type,
        });
    }
};