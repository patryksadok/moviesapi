const moviesRepository = require('../repositories/moviesRepository');
const MovieDoesNotExistError = require('../errors/movieNotFoundError');

module.exports.checkIfMovieExist = async (req, res, next) => {
    try {
        const { movieId } = req.params;

        const movie = await moviesRepository.findMovieById(movieId);

        if (!movie) {
            throw new MovieDoesNotExistError();
        }

        return next();
    }
    catch (error) {
        console.log(error);
         return res.status(error.status).json({
            message: error.message,
            type: error.type,
        });
    }
}