const moviesRepository = require('../repositories/moviesRepository');
const MovieAlreadyExists = require('../errors/movieAlreadyExistsError');

module.exports.checkIfMovieExist = async (req, res, next) => {
    try {
        const { imdbId } = req.body;

        const movie = await moviesRepository.findMovie(imdbId);

        if (movie) {
            throw new MovieAlreadyExists();
        }

        return next();
    }
    catch (error) {
        console.log(error);
         return res.status(error.status).json({
            message: error.message,
            type: error.type,
        });
    }
}