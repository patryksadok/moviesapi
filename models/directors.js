'use strict';

module.exports = (sequelize, DataTypes) => {
    const director = sequelize.define('directors', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        firstName: {
            type: DataTypes.STRING,
            name: 'firstName',
            field: 'first_name'
        },
        secondName: {
            type: DataTypes.STRING,
            name: 'secondName',
            field: 'second_name'
        },
        createdAt: {
            type: DataTypes.DATE,
            defaultValue: DataTypes.NOW,
            name: 'createdAt',
            field: 'created_at'
        },
        updatedAt: {
            type: DataTypes.DATE,
            name: 'updatedAt',
            field: 'updated_at'
        }
    }, {
        underscored: true,
    });

    director.associate = models => {
    };

    return director;
};
