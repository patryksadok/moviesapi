'use strict';
const directors = require('./directors');
const productions = require('./productions');

module.exports = (sequelize, DataTypes) => {
    const movie = sequelize.define('movies', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        title: {
            type: DataTypes.STRING
        },
        year: {
            type: DataTypes.INTEGER
        },
        rated: {
            type: DataTypes.STRING
        },
        released: {
            type: DataTypes.DATE
        },
        runtime: {
            type: DataTypes.STRING
        },
        writer: {
            type: DataTypes.STRING
        },
        plot: {
            type: DataTypes.STRING
        },
        language: {
            type: DataTypes.STRING
        },
        country: {
            type: DataTypes.STRING
        },
        poster: {
            type: DataTypes.STRING
        },
        metascore: {
            type: DataTypes.INTEGER
        },
        imdbRating: {
            type: DataTypes.INTEGER,
            name: 'imdbRating',
            field: 'imdb_rating'
        },
        imdbVotes: {
            type: DataTypes.STRING,
            name: 'imdbVotes',
            field: 'imdb_votes'
        },
        imdbId: {
            type: DataTypes.STRING,
            name: 'imdbId',
            field: 'imdb_id'
        },
        type: {
            type: DataTypes.STRING
        },
        dvd: {
            type: DataTypes.DATE
        },
        boxOffice: {
            type: DataTypes.STRING,
            name: 'boxOffice',
            field: 'box_office'
        },
        website: {
            type: DataTypes.STRING
        },
        createdAt: {
            type: DataTypes.DATE,
            defaultValue: DataTypes.NOW,
            name: 'createdAt',
            field: 'created_at'
        },
        updatedAt: {
            type: DataTypes.DATE,
            name: 'updatedAt',
            field: 'updated_at'
        },
        directorId: {
            allowNull: false,
            type: DataTypes.INTEGER,
            name: 'directorId',
            field: 'director_id',
            references: {
                model: directors,
                key: 'id',
            }
        },
        productionId: {
            allowNull: false,
            type: DataTypes.INTEGER,
            name: 'productionId',
            field: 'production_id',
            references: {
                model: productions,
                key: 'id',
            }
        }
    }, {
        underscored: true,
    });

    movie.associate = models => {
    };

    return movie;
};
