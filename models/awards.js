'use strict';
const movies = require('./movies');

module.exports = (sequelize, DataTypes) => {
    const award = sequelize.define('awards', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        type: {
            type: DataTypes.STRING
        },
        movieId: {
            allowNull: false,
            type: DataTypes.INTEGER,
            name: 'movieId',
            field: 'movie_id',
            references: {
                model: movies,
                key: 'id',
            }
        },
        createdAt: {
            type: DataTypes.DATE,
            defaultValue: DataTypes.NOW,
            name: 'createdAt',
            field: 'created_at'
        },
        updatedAt: {
            type: DataTypes.DATE,
            name: 'updatedAt',
            field: 'updated_at'
        }
    }, {
        underscored: true,
    });

    award.associate = models => {
    };

    return award;
};
