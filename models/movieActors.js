'use strict';
const movies = require('./movies');
const actors = require('./actors');

module.exports = (sequelize, DataTypes) => {
    const movieActor = sequelize.define('movieActors', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        actorId: {
            allowNull: false,
            type: DataTypes.INTEGER,
            name: 'actorId',
            field: 'actor_id',
            references: {
                model: actors,
                key: 'id',
            }
        },
        movieId: {
            allowNull: false,
            type: DataTypes.INTEGER,
            name: 'movisId',
            field: 'movie_id',
            references: {
                model: movies,
                key: 'id',
            }
        },

    }, {
        underscored: true,
    });

    movieActor.associate = models => {
    };

    return movieActor;
};
