'use strict';
const movies = require('./movies');
const genres = require('./genres');

module.exports = (sequelize, DataTypes) => {
    const movieGenre = sequelize.define('movieGenres', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        genreId: {
            allowNull: false,
            type: DataTypes.INTEGER,
            name: 'genreId',
            field: 'genre_id',
            references: {
                model: genres,
                key: 'id',
            }
        },
        movieId: {
            allowNull: false,
            type: DataTypes.INTEGER,
            name: 'movisId',
            field: 'movie_id',
            references: {
                model: movies,
                key: 'id',
            }
        },

    }, {
        underscored: true,
    });

    movieGenre.associate = models => {
    };

    return movieGenre;
};
