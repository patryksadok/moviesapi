module.exports = {
    HTTP_200_OK: 200,
    HTTP_400_BAD_REQUEST: 400,
    HTTP_404_NOT_FOUND: 404,
    HTTP_500_SERVER_FAIL: 500,
};
